import requests
import json


class EDSMAPI:
    def __init__(self):
        self._base_url = "https://www.edsm.net"

    """ Get the current list of discared events"""
    def get_ignored_events(self):
        url = self._base_url + '/api-journal-v1/discard'
        r = requests.get(url)
        if r.status_code == 200:
            return r.text
        else:
            raise Exception("Unable to connect to EDSM..")

    """ Posts a journal entry to EDSM """
    def post_journal_entry(self, entry):
        url = self._base_url + "/api-journal-v1"
        r = requests.post(url, data=entry)
        response = json.loads(r.text)
        if response['msgnum'] == 100:
            return
        else:
            raise Exception(r.text)

    """ Get the system info based on the json object given.
        Params:
            system: A dict containing the key ssystemName and
                    optionally systemId
        Return:
            a new JSON object containing all the info about the system. """
    def get_system_info(self, system):
        url = self._base_url + '/api-system-v1/bodies'
        r = requests.get(url, params=system)
        return json.loads(r.text)

    """ Get the system scan value.
        Params:
            system: A dict containing the key systemName and
                    optionally systemId
        Return:
            a new JSON object containing all the info about the system. """
    def get_system_value(self, system):
        url = self._base_url + '/api-system-v1/estimated-value'
        r = requests.get(url, params=system)
        return json.loads(r.text)
