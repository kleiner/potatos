import json
import utils
from gamestate import GameState
from edsmapi import EDSMAPI


""" Class that parses Journal entries and keeps track of game state """


class JournalParser():
    def __init__(self, cmdr_name, api_key):
        self.api = EDSMAPI()
        self.game_state = GameState()

        discard_string = self.api.get_ignored_events()
        self.discard_list = json.loads(discard_string)

        self.cmdr_name = cmdr_name
        self.api_key = api_key
        self._software_name = utils.get_software_name()
        self._software_version = utils.get_software_version()
        self._body_mapped = False

    """ Parses a Journal entry """
    def parse_entry(self, entry):
        parsed = json.loads(entry)
        parsed = self.game_state.update_game_state(parsed)

        if parsed['event'] not in self.discard_list and \
           self.game_state.send_events:
            json_obj = self.build_json_object(json.dumps(parsed))
            self.send_entry(json_obj)

        # since we can't see in the Scan event that we just mapped a planet
        # set our own check
        if parsed['event'] == 'SAAScanComplete':
            self._body_mapped = True

        # once the whole system is scanned, check for valuables bodies via EDSM
        if parsed['event'] == 'FSSAllBodiesFound':
            system = {'systemName': parsed['SystemName']}
            value = self.api.get_system_value(system)
            if value['valuableBodies']:
                for body in value['valuableBodies']:
                    print(body['bodyName'] + " has an estimated worth of " +
                          str(body['valueMax']))

        # non scoopable star at the other side?
        if parsed['event'] == 'StartJump' and \
           parsed['JumpType'] == 'Hyperspace':
            if parsed['StarClass'] not in 'OBAFGKM':
                print("Target star is NOT scoopable")

    """ Builds an object that is ready to be posted to EDSM """
    def build_json_object(self, entry):
        return {'commanderName': self.cmdr_name,
                'apiKey': self.api_key,
                'fromSoftware': self._software_name,
                'fromSoftwareVersion': self._software_version,
                'message': entry}

    """ Tries to send the entry to EDSM """
    def send_entry(self, json_obj):
        try:
            self.api.post_journal_entry(json_obj)
            # print(json_obj + " sent to EDSM")
        except Exception as e:
            print(e.args)
