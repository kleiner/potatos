Some horrible code supposed to help out while playing Elite: Dangerous.  
Features: 
* Sends Journal entries to EDSM.
* Uses EDSM to get valuable planets in a system, need to finish scanning the
whole system.
* Warns when jumping to a star that is not scoopable.


# Setup
To make it run, install the required libraries listed in requirements.txt.
You need to export your Commander Name and EDSM API key to EDCMDRNAME and
EDSMAPIKEY respectivelly, otherwise the software will crash.

Once that is done, run `python3 src/potatos.py` and start the game.  
Only tested on Linux, success on your system is not guaranteed.
