import math
# See
# https://forums.frontier.co.uk/threads/exploration-value-formulae.232000/


def get_interesting_types():
    return ['Metal-rich body', 'Ammonia world', 'High metal content world',
            'Earth-like world', 'Water world']


def get_k_value(planet_type, terraformable):
    base = 300
    if terraformable:
        bonus = 93328
    else:
        bonus = 0

    if planet_type == "Metal-rich body":
        base = 21790
        if terraformable:
            bonus = 65631
        else:
            bonus = 0

    if planet_type == "Ammonia world":
        base = 96932
        bonus = 0

    if planet_type == "Class I gas giant":
        base = 1656
        bonus = 0

    if planet_type in ["High metal content world", "Class II gas giant"]:
        base = 9654
        if terraformable:
            bonus = 100677
        else:
            bonus = 0

    if planet_type in ["Earth-like world", "Water world"]:
        base = 64831
        if terraformable or planet_type == "Earth-like world":
            bonus = 116295
        else:
            bonus = 0

    return base + bonus


def get_estimated_value(planet_type, mass, terraformable):
    q = 0.56591828
    k = get_k_value(planet_type, terraformable)
    mapping_mult = 3.3333333333 * 1.25

    return max(500, math.floor((k + k * q * (mass ** 0.2)) * mapping_mult))
