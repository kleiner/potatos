""" A simple class to keep track of the game state
    Holy fuck I hate this thing... Glad I made it into it's own class! """


class GameState:
    def __init__(self):
        self.system_address = None      # systemId
        self.system_name = None         # system
        self.system_coordinates = None  # coordinates
        self.market_id = None           # stationId
        self.station_name = None        # station
        self.ship_id = None             # ship.id
        self.cmdr_name = None
        self.send_events = True

    """ Takes a parsed JSON entry and updates the internal game state """
    def update_game_state(self, entry):
        if entry['event'] == "LoadGame":
            self.system_address = None
            self.system_name = None
            self.system_coordinates = None
            self.market_id = None
            self.station_name = None
            self.ship_id = None
            self.cmdr_name = entry['Commander']
            self.send_events = True

        if entry['event'] == "SetUserShipName":
            self.ship_id = entry['ShipID']

        if entry['event'] == "ShipyardBuy":
            self.ship_id = None

        if entry['event'] == "ShipyardSwap":
            self.ship_id = entry['ShipID']

        if entry['event'] == "LoadOut":
            self.ship_id = entry['ShipID']

        if entry['event'] == "Undocked":
            self.market_id = None
            self.station_name = None

        if entry['event'] in ["Location", "FSDJump", "Docked"]:
            # docked don't have coordinates, if system changed reset
            if entry['StarSystem'] != self.system_name:
                self.system_coordinates = None

            if entry['StarSystem'] not in ['ProvingGround', 'CQC']:
                if 'SystemAddress' in entry:
                    self.system_address = entry['SystemAddress']

                self.system_name = entry['StarSystem']

                if 'StarPos' in entry:
                    self.system_coordinates = entry['StarPos']
            else:
                self.system_address = None
                self.system_name = None
                self.system_coordinates = None

            if 'MarketId' in entry:
                self.market_id = entry['MarketId']

            if 'StationName' in entry:
                self.station_name = entry['StationName']

        if entry['event'] in ['JoinACrew', 'QuitACrew']:
            if entry['event'] == 'JoinACrew' and \
               entry['Captain'] != self.cmdr_name:
                self.send_events = False
            else:
                self.send_events = True

            self.system_address = None      # systemId
            self.system_name = None         # system
            self.system_coordinates = None  # coordinates
            self.market_id = None           # stationId
            self.station_name = None        # station

        entry['_systemAddress'] = self.system_address
        entry['_systemName'] = self.system_name
        entry['_systemCoordinates'] = self.system_coordinates
        entry['_marketId'] = self.market_id
        entry['_stationName'] = self.station_name
        entry['_shipId'] = self.ship_id

        return entry
