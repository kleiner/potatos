import os
import glob


def get_journal_dir():
    return os.path.expanduser(("~/.steam/steam/steamapps/compatdata/359320/"
                               "pfx/drive_c/users/steamuser/Saved Games/"
                               "Frontier Developments/Elite Dangerous"))


def get_latest_journal():
    list_of_journals = glob.glob(get_journal_dir() + '/*.log')
    while not list_of_journals:
        list_of_journals = glob.glob(get_journal_dir() + '/*.log')

    latest_journal = max(list_of_journals, key=os.path.getctime)
    return latest_journal


def get_software_version():
    return "1.0.0"


def get_software_name():
    return "POTATOS"
