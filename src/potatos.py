import os
import time
from pygtail import Pygtail
import journalparser
import utils


def main():
    journal = Pygtail(utils.get_latest_journal())
    count_end_reached = 0
    parser = journalparser.JournalParser(os.environ['EDCMDRNAME'],
                                         os.environ['EDSMAPIKEY'])

    while True:
        try:
            parser.parse_entry(journal.next())
        except StopIteration:
            if count_end_reached > 30:  # aka, waited a minute
                count_end_reached = 0
                new_journal = utils.get_latest_journal()
                if new_journal != journal.filename:
                    journal = Pygtail(new_journal)
            else:  # reached the end of the file, wait 2 seconds
                count_end_reached += 1
                time.sleep(2)


if __name__ == "__main__":
    main()
